package si.uni_lj.fri.pbd.miniapp3.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import si.uni_lj.fri.pbd.miniapp3.R;

/*
utility viewholder class for recyclerview elements
 */

public class RecipeViewHolder extends RecyclerView.ViewHolder {
    private ImageView recipeimg;
    private TextView recipetxt;

    public RecipeViewHolder(final View recipeView){
        super(recipeView);
        recipeimg=recipeView.findViewById(R.id.image_view);
        recipetxt=recipeView.findViewById(R.id.text_view_content);
    }

    public TextView getRecipetxt() {
        return recipetxt;
    }

    public ImageView getRecipeimg() {
        return recipeimg;
    }
}
