package si.uni_lj.fri.pbd.miniapp3.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.database.RecipeViewModel;
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;
import si.uni_lj.fri.pbd.miniapp3.models.Mapper;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeDetailsIM;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeDetailsDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIdDTO;
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI;
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator;

public class DetailsActivity extends AppCompatActivity {

    private static final int SEARCH=0;
    private static final int FAVORITE=1;
    private String id;
    private ImageView img;
    private TextView title, misc, ingredients, instructions, ytube;
    private Button button;
    private Bitmap bitmap;
    private RestAPI api;
    private RecipeDetailsIM recipeim;
    private SharedPreferences sp;
    private RecipeViewModel rvm;

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        //set everything up
        getapi();
        sp=getApplicationContext().getSharedPreferences("favoriteimgs", Context.MODE_PRIVATE);
        img=findViewById(R.id.details_image_view);
        title=findViewById(R.id.details_title);
        misc=findViewById(R.id.details_misc);
        ingredients=findViewById(R.id.details_ingredients);
        instructions=findViewById(R.id.details_instructions);
        ytube=findViewById(R.id.details_ytube);
        button=findViewById(R.id.details_button);
        button.setVisibility(View.INVISIBLE);

        //get viewmodel
        rvm=new ViewModelProvider(this).get(RecipeViewModel.class);

        //get extra info
        Intent i=getIntent();
        int where = i.getIntExtra("where", 0);
        id=i.getStringExtra("id");

        //either fetch from api or DB+shared pref
        if(where==SEARCH){
            download();
        } else if(where==FAVORITE){
            fetch();
        }
    }

    //add recipeim to DB and bitmap to shared pref
    private void addfav(){
        rvm.insert(Mapper.mapRecipeDetailsIMToRecipeDetails(true,recipeim));
        SharedPreferences.Editor eddie=sp.edit();
        eddie.putString(recipeim.getIdMeal(),BitMapToString(bitmap));
        eddie.apply();
    }

    //remove recipeim from DB and bitmap from shared pref
    private void removefav(String id){
        rvm.delete(id);
        SharedPreferences.Editor eddie=sp.edit();
        eddie.remove(recipeim.getIdMeal());
        eddie.apply();
    }

    //set button to add favorite
    private void setbuttonadd(){
        button.setVisibility(View.VISIBLE);
        button.setText(getString(R.string.addtofav));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addfav();
                setbuttonremove();
            }
        });
    }

    //set button to remove favorite
    private void setbuttonremove(){
        button.setVisibility(View.VISIBLE);
        button.setText(getString(R.string.removefav));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removefav(recipeim.getIdMeal());
                setbuttonadd();
            }
        });
    }

    //fetch recipe from db
    private void fetch(){
        rvm.find(id);
        rvm.getSearchRes().observe(this, new Observer<RecipeDetails>() {
            @Override
            public void onChanged(RecipeDetails recipeDetails) {
                if(recipeDetails!=null){
                    //map to IM from DB class, get bitmap from shared pref, set the views to those values
                    recipeim=Mapper.mapRecipeDetailsToRecipeDetailsIm(recipeDetails.getFavorite(),recipeDetails);
                    String encodedbmap=sp.getString(recipeim.getIdMeal(),null);
                    if(encodedbmap!=null){
                        bitmap=StringToBitMap(encodedbmap);
                        if(bitmap!=null){
                            img.setImageBitmap(bitmap);
                        }
                    }
                    setviews();
                    //set button functionality
                    if(recipeim.getFavorite()){
                        setbuttonremove();
                    } else{
                        setbuttonadd();
                    }
                } else{
                    //let the user know
                    Toast to=Toast.makeText(getApplicationContext(),getResources().getString(R.string.deterror),Toast.LENGTH_LONG);
                    to.setGravity(Gravity.BOTTOM,0,0);
                    to.show();
                }
            }
        });
    }

    //utility to get restapi
    private void getapi(){
        if(api==null){
            api= ServiceGenerator.createService(RestAPI.class);
        }
    }

    //set views to IM values, build youtube link, build ingredients string
    private void setviews(){
        title.setText(recipeim.getStrMeal());
        misc.setText(recipeim.getStrArea());
        instructions.setText(recipeim.getStrInstructions());
        //works like hyperlink
        String ytlink="<html><a href=\""+recipeim.getStrYoutube()+"\">Watch on youtube</a></html>";
        ytube.setText(Html.fromHtml(ytlink));
        ytube.setMovementMethod(LinkMovementMethod.getInstance());
        //JSON arrays though :'(
        String placeholder=recipeim.getStrIngredient1();
        String ingr="";
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(R.string.noingredients);
            return;
        }
        ingr+=placeholder+": "+recipeim.getStrMeasure1();
        placeholder=recipeim.getStrIngredient2();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure2();
        placeholder=recipeim.getStrIngredient3();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure3();
        placeholder=recipeim.getStrIngredient4();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure4();
        placeholder=recipeim.getStrIngredient5();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure5();
        placeholder=recipeim.getStrIngredient6();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure6();
        placeholder=recipeim.getStrIngredient7();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure7();
        placeholder=recipeim.getStrIngredient8();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure8();
        placeholder=recipeim.getStrIngredient9();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure9();
        placeholder=recipeim.getStrIngredient10();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure10();
        placeholder=recipeim.getStrIngredient11();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure11();
        placeholder=recipeim.getStrIngredient12();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure12();
        placeholder=recipeim.getStrIngredient13();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure13();
        placeholder=recipeim.getStrIngredient14();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure14();
        placeholder=recipeim.getStrIngredient15();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure15();
        placeholder=recipeim.getStrIngredient16();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure16();
        placeholder=recipeim.getStrIngredient17();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure17();
        placeholder=recipeim.getStrIngredient18();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure18();
        placeholder=recipeim.getStrIngredient19();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure19();
        placeholder=recipeim.getStrIngredient20();
        if(placeholder==null||placeholder.equals("")){
            ingredients.setText(ingr);
            return;
        }
        ingr+=", "+placeholder+": "+recipeim.getStrMeasure20();
        ingredients.setText(ingr);
    }

    //updates existing DB entry with fresh data
    private void updatedirty(){
        rvm.find(id);
        rvm.getSearchRes().observe(this,new Observer<RecipeDetails>(){
            @Override
            public void onChanged(RecipeDetails recipeDetails) {
                if(recipeDetails!=null){
                    setbuttonremove();
                    rvm.update(Mapper.mapRecipeDetailsIMToRecipeDetails(recipeDetails.getFavorite(),recipeim));
                } else{
                    setbuttonadd();
                }
            }
        });
    }

    //fetches recipe details from api
    private void download(){
        getapi();
        Call<RecipesByIdDTO> recipeDetailsDTOCall=api.getByMealID(id);
        recipeDetailsDTOCall.enqueue(new Callback<RecipesByIdDTO>() {
            @Override
            public void onResponse(@NotNull Call<RecipesByIdDTO> call, @NotNull Response<RecipesByIdDTO> response) {
                if(response.isSuccessful()&&response.body()!=null){
                    //map to IM class, get bitmap, set views, possibly update DB entry
                    RecipesByIdDTO dto=response.body();
                    RecipeDetailsDTO recipedto=dto.getMeals().get(0);
                    assert recipedto!=null;
                    recipeim=Mapper.mapRecipeDetailsDtoToRecipeDetailsIm(false,recipedto);
                    new Downloader().execute(recipeim.getStrMealThumb());
                    setviews();
                    updatedirty();
                }
            }

            @Override
            public void onFailure(@NotNull Call<RecipesByIdDTO> call, @NotNull Throwable t) {
                //let the user know
                Toast to=Toast.makeText(getApplicationContext(),getResources().getString(R.string.deterror),Toast.LENGTH_LONG);
                to.setGravity(Gravity.BOTTOM,0,0);
                to.show();
                call.cancel();
            }
        });
    }

    //utility bitmap->base64
    private String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100, baos);
        return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
    }

    //utility base64->string
    private Bitmap StringToBitMap(String encodedString){
        if(encodedString==null){
            return null;
        }
        try {
            byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(Base64.decode(encodedString, Base64.DEFAULT), 0, encodeByte.length);
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }

    private class Downloader extends AsyncTask<String,Void,Bitmap> {

        protected void onPostExecute(Bitmap bmap){
            bitmap=bmap;
            img.setImageBitmap(bmap);
        }

        //fetches bitmap from url
        @Override
        protected Bitmap doInBackground(String... strings) {
            Bitmap bmap;
            URL url;
            InputStream in;
            BufferedInputStream buf;
            try{
                url = new URL(strings[0]);
                in=url.openStream();
                buf=new BufferedInputStream(in);
                bmap= BitmapFactory.decodeStream(buf);
                if(in!=null){
                    in.close();
                }
                buf.close();
                return bmap;
            } catch (Exception e) {
                Log.e("Error reading file", e.toString());
            }
            return null;
        }
    }
}
