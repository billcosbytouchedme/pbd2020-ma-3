package si.uni_lj.fri.pbd.miniapp3.adapter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;
import si.uni_lj.fri.pbd.miniapp3.ui.DetailsActivity;
import si.uni_lj.fri.pbd.miniapp3.viewholders.RecipeViewHolder;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecipeViewHolder> {

    private List<RecipeSummaryIM> recipes;
    private List<Bitmap> images;
    private int where;

    public RecyclerViewAdapter(int where){
        this.where=where;
    }

    //populates adapter with new dataset
    public void setrecipes(List<RecipeSummaryIM> recipes, List<Bitmap> images){
        this.recipes=recipes;
        this.images=images;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecipeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_grid_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeViewHolder holder, int position) {
        //assigning dataset values to holder
        holder.getRecipetxt().setText(recipes.get(position).getStrMeal());
        //check if image was successfuly retrieved
        if(images.get(position)!=null){
            holder.getRecipeimg().setImageBitmap(images.get(position));
        }
        //linking to details activity
        holder.getRecipeimg().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(v.getContext(), DetailsActivity.class);
                i.putExtra("id",recipes.get(position).getIdMeal());
                i.putExtra("where",where);
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return recipes==null?0:recipes.size();
    }
}
