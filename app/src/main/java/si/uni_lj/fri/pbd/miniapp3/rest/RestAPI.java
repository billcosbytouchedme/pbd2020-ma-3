package si.uni_lj.fri.pbd.miniapp3.rest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientsDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIdDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIngredientDTO;

public interface RestAPI {

    // URL/list.php?i=list
    @GET("list.php?i=list")
    Call<IngredientsDTO> getAllIngredients();

    // URL/filter.php?i=ingredientname
    @GET("filter.php")
    Call<RecipesByIngredientDTO> getByMainIngredient(@Query("i") String ingredientname);

    // URL/lookup.php?i=id
    @GET("lookup.php")
    Call<RecipesByIdDTO> getByMealID(@Query("i") String id);

}