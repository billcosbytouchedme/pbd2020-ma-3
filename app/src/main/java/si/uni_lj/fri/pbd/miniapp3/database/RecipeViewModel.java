package si.uni_lj.fri.pbd.miniapp3.database;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;

public class RecipeViewModel extends AndroidViewModel {

    private LiveData<List<RecipeDetails>> all;
    private MutableLiveData<RecipeDetails> searchRes;
    private RecipeRepo repo;

    public RecipeViewModel(@NonNull Application application) {
        super(application);
        repo=new RecipeRepo(application);
        all=repo.getAll();
        searchRes=repo.getSearchRes();
    }

    public LiveData<List<RecipeDetails>> getAll() {
        return all;
    }

    public MutableLiveData<RecipeDetails> getSearchRes() {
        return searchRes;
    }

    public void insert(RecipeDetails rd){
        repo.insert(rd);
    }

    public void update(RecipeDetails rd){
        repo.update(rd);
    }

    public void delete(String id){
        repo.delete(id);
    }

    public void find(String id){
        repo.find(id);
    }
}
