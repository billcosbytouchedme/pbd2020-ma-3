package si.uni_lj.fri.pbd.miniapp3.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;

@Dao
public interface RecipeDao {

    //insert, update, delete and getbyid for details activity, getall for favorites fragment

    @Insert
    void insertRecipe(RecipeDetails recipeDetails);

    @Update
    void updateRecipe(RecipeDetails recipeDetails);

    @Query("SELECT * FROM RecipeDetails WHERE idMeal = :idMeal")
    RecipeDetails getRecipeById(String idMeal);

    @Query("DELETE FROM RecipeDetails WHERE idMeal = :idMeal")
    void deleteRecipeById(String idMeal);

    @Query("SELECT * FROM RecipeDetails")
    LiveData<List<RecipeDetails>> getAllRecipes();

}
