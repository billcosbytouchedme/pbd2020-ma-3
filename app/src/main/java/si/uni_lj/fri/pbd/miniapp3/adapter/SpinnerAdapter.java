package si.uni_lj.fri.pbd.miniapp3.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientsDTO;
import si.uni_lj.fri.pbd.miniapp3.viewholders.SpinViewHolder;

public class SpinnerAdapter extends BaseAdapter {

    private IngredientsDTO ingredients;

    public SpinnerAdapter(){

    }

    //populate adapter with new dataset
    public void setIngredients(IngredientsDTO ingredients){
        this.ingredients=ingredients;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return ingredients==null?0:ingredients.getIngredients().size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    //assigning dataset entry to viewholder or making a new one first because BaseAdapter doesn't bind them for oyu
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        IngredientDTO ingredient=ingredients.getIngredients().get(position);
        SpinViewHolder holder;
        if(view==null){
            view=LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item,parent,false);
            holder=new SpinViewHolder(view.findViewById(R.id.text_view_spinner_item));
            view.setTag(holder);
        } else{
            holder=(SpinViewHolder)view.getTag();
        }
        holder.setText(ingredient.getStrIngredient());
        return view;
    }
}
