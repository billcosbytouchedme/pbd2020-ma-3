package si.uni_lj.fri.pbd.miniapp3.search;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter;
import si.uni_lj.fri.pbd.miniapp3.adapter.SpinnerAdapter;
import si.uni_lj.fri.pbd.miniapp3.models.Mapper;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientsDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeSummaryDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI;
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator;
import si.uni_lj.fri.pbd.miniapp3.viewholders.SpinViewHolder;

public class SearchFragment extends Fragment implements AdapterView.OnItemSelectedListener, SwipeRefreshLayout.OnRefreshListener {

    private static final int SEARCH=0;
    private RestAPI api;
    private RecyclerViewAdapter rvadapter;
    private long timestamp=0L;
    private String selected;
    private SwipeRefreshLayout srl;
    private List<Bitmap> images;
    private RecipesByIngredientDTO dto;
    private MaterialProgressBar mpb;
    private SpinnerAdapter sa;

    public SearchFragment(){

    }

    //setting everything up
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_search,container,false);

        //grid layout 2 per row, setting up adapter
        RecyclerView recipes=view.findViewById(R.id.recyclerfavorites);
        recipes.setLayoutManager(new GridLayoutManager(getContext(),2));
        rvadapter=new RecyclerViewAdapter(SEARCH);
        recipes.setAdapter(rvadapter);

        //setting up swipe refresh
        srl=view.findViewById(R.id.swipeRefreshLayout);
        srl.setOnRefreshListener(this);

        //setting up spinner
        Spinner spinner=view.findViewById(R.id.spinner);
        sa=new SpinnerAdapter();

        //fetch ingredients from api, show loading circle
        mpb=view.findViewById(R.id.progress);
        mpb.setVisibility(View.VISIBLE);
        loadingredients();

        //setting up spinner
        spinner.setOnItemSelectedListener(this);
        spinner.setAdapter(sa);
        return view;
    }

    //utility to fetch restapi
    private void getapi(){
        if(api==null){
            api=ServiceGenerator.createService(RestAPI.class);
        }
    }

    //fetch ingredients from api
    private void loadingredients(){
        getapi();
        Call<IngredientsDTO> ingredientsDTOCall=api.getAllIngredients();
        ingredientsDTOCall.enqueue(new Callback<IngredientsDTO>() {
            @Override
            public void onResponse(@NotNull Call<IngredientsDTO> call, @NotNull Response<IngredientsDTO> response) {
                //populate adapter with fresh dataset, hide loading circle
                if(response.isSuccessful()&&response.body()!=null) {
                    IngredientsDTO ingredientsDTO = response.body();
                    assert ingredientsDTO != null;
                    sa.setIngredients(ingredientsDTO);
                    mpb.setVisibility(View.INVISIBLE);
                } else{
                    //let the user know, hide loading circle
                    FragmentActivity fa=getActivity();
                    assert fa!=null;
                    Toast to=Toast.makeText(fa.getApplicationContext(),getResources().getString(R.string.ingerror),Toast.LENGTH_LONG);
                    to.setGravity(Gravity.BOTTOM,0,0);
                    to.show();
                    mpb.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(@NotNull Call<IngredientsDTO> call, @NotNull Throwable t) {
                //let the user know, hide loading circle
                FragmentActivity fa=getActivity();
                assert fa!=null;
                Toast to=Toast.makeText(fa.getApplicationContext(),getResources().getString(R.string.ingerror),Toast.LENGTH_LONG);
                to.setGravity(Gravity.BOTTOM,0,0);
                to.show();
                mpb.setVisibility(View.INVISIBLE);
                call.cancel();
            }
        });
    }

    //fetch recipes from api
    private void loadRecipes(String ingredient){
        getapi();
        images=new ArrayList<>();
        //show loading circle
        mpb.setVisibility(View.VISIBLE);
        Call<RecipesByIngredientDTO> recipesByIngredientDTOCall=api.getByMainIngredient(ingredient);
        recipesByIngredientDTOCall.enqueue(new Callback<RecipesByIngredientDTO>() {
            @Override
            public void onResponse(@NotNull Call<RecipesByIngredientDTO> call, @NotNull Response<RecipesByIngredientDTO> response) {
                if(response.isSuccessful()&&response.body()!=null){
                    //update stamp
                    timestamp=System.currentTimeMillis();
                    dto=response.body();
                    //compile list of dtos and images to populate adapter with
                    List<RecipeSummaryDTO> recipes=dto.getMeals();
                    if(recipes!=null){
                        int n=recipes.size();
                        String[] urls=new String[n];
                        for(int i=0;i<n;i++){
                            urls[i]=recipes.get(i).getStrMealThumb();
                        }
                        new Downloader().execute(urls);
                    } else{
                        //no recipes found
                        notifyadapter(new ArrayList<>());
                    }
                } else{
                    //let the user know, hide loading circle
                    FragmentActivity fa=getActivity();
                    assert fa!=null;
                    Toast to=Toast.makeText(fa.getApplicationContext(),getResources().getString(R.string.recerror),Toast.LENGTH_LONG);
                    to.setGravity(Gravity.BOTTOM,0,0);
                    to.show();
                    mpb.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(@NotNull Call<RecipesByIngredientDTO> call, @NotNull Throwable t) {
                //let the user know, hide loading circle
                FragmentActivity fa=getActivity();
                assert fa!=null;
                Toast to=Toast.makeText(fa.getApplicationContext(),getResources().getString(R.string.recerror),Toast.LENGTH_LONG);
                to.setGravity(Gravity.BOTTOM,0,0);
                to.show();
                mpb.setVisibility(View.INVISIBLE);
                call.cancel();
            }
        });
    }

    //spinner item selected
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //get recipes with new specified ingredient
        SpinViewHolder holder= (SpinViewHolder) view.getTag();
        selected=holder.getText();
        loadRecipes(selected);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //don't care
    }

    //swipe refresh triggered
    @Override
    public void onRefresh() {
        //stop the double loading circle, check if 5 seconds passed and refresh everything
        srl.setRefreshing(false);
        if(System.currentTimeMillis()-timestamp>=5000L){
            mpb=getView().findViewById(R.id.progress);
            mpb.setVisibility(View.VISIBLE);
            loadingredients();
            loadRecipes(selected);
        }
    }

    //populate adapter with fresh dataset
    private void notifyadapter(List<Bitmap> imgs){
        images=imgs;
        rvadapter.setrecipes(Mapper.mapRecipesByIngredientDTOtoListRecipeSummaryIM(dto),images);
        mpb.setVisibility(View.INVISIBLE);
        if(dto.getMeals()==null){
            //let the user know
            FragmentActivity fa=getActivity();
            assert fa!=null;
            Toast t=Toast.makeText(fa.getApplicationContext(),"No recipes with that ingredient were found.",Toast.LENGTH_LONG);
            t.setGravity(Gravity.BOTTOM,0,0);
            t.show();
        }
    }

    private class Downloader extends AsyncTask<String,Void,List<Bitmap>>{
        protected void onPostExecute(List<Bitmap> img){
            notifyadapter(img);
        }

        @Override
        protected List<Bitmap> doInBackground(String... urls) {
            List<Bitmap> ret=new ArrayList<>();
            URL url;
            InputStream in;
            BufferedInputStream buf;
            //try to get bitmaps from urls one at a time, compile into list, insert null if download fails
            for(int i=0;i<urls.length;i++){
                try{
                    url = new URL(urls[i]);
                    in=url.openStream();
                    buf=new BufferedInputStream(in);
                    Bitmap bmap= BitmapFactory.decodeStream(buf);
                    if(in!=null){
                        in.close();
                    }
                    buf.close();
                    ret.add(i,bmap);
                } catch (Exception e) {
                    Log.e("Error reading file", e.toString());
                }
            }
            return ret;
        }
    }
}
