package si.uni_lj.fri.pbd.miniapp3.viewholders;

import android.widget.TextView;

/*
utility viewholder class for spinner elements
 */

public class SpinViewHolder {
    private TextView text;

    public SpinViewHolder(TextView txt){
        this.text=txt;
    }

    public void setText(String txt){
        this.text.setText(txt);
    }

    public String getText(){
        return (String)this.text.getText();
    }
}
