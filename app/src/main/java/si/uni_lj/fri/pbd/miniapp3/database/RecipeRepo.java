package si.uni_lj.fri.pbd.miniapp3.database;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.database.dao.RecipeDao;
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;

public class RecipeRepo {
    private MutableLiveData<RecipeDetails> search=new MutableLiveData<>();
    private LiveData<List<RecipeDetails>> all;
    private RecipeDao dao;

    public RecipeRepo(Application app){
        Database db=Database.getDatabase(app);
        dao=db.recipeDao();
        all=dao.getAllRecipes();
    }

    public void update(final RecipeDetails rd){
        Database.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                dao.updateRecipe(rd);
            }
        });
    }

    public void insert(final RecipeDetails rd){
        Database.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                dao.insertRecipe(rd);
            }
        });
    }

    public void delete(final String id){
        Database.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                dao.deleteRecipeById(id);
            }
        });
    }

    public void find(final String id){
        Database.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                search.postValue(dao.getRecipeById(id));
            }
        });
    }

    public LiveData<List<RecipeDetails>> getAll(){
        return all;
    }

    public MutableLiveData<RecipeDetails> getSearchRes() {
        return search;
    }
}
