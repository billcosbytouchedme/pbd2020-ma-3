package si.uni_lj.fri.pbd.miniapp3.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import si.uni_lj.fri.pbd.miniapp3.favorites.FavoritesFragment;
import si.uni_lj.fri.pbd.miniapp3.search.SearchFragment;

public class SectionsPagerAdapter extends FragmentStateAdapter {

    private int tabs;

    public SectionsPagerAdapter(@NonNull FragmentActivity fa, int numTabs) {
        super(fa);
        tabs=numTabs;
    }

    //map tabs to the 2 fragments
    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0:
                return new SearchFragment();
            case 1:
                return new FavoritesFragment();
            default:
                return null;
        }
    }

    @Override
    public int getItemCount() {
        return tabs;
    }
}
