package si.uni_lj.fri.pbd.miniapp3.favorites;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter;
import si.uni_lj.fri.pbd.miniapp3.database.RecipeViewModel;
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;
import si.uni_lj.fri.pbd.miniapp3.models.Mapper;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;

public class FavoritesFragment extends Fragment {

    private static final int FAVORITE=1;
    private RecyclerViewAdapter rvadapter;
    private SharedPreferences sp;

    public FavoritesFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_favorites,container,false);
    }

    //setting everything up
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //grid layout 2 per row, setting up adapter
        rvadapter=new RecyclerViewAdapter(FAVORITE);
        View v=getView();
        assert v!=null;
        RecyclerView rv=v.findViewById(R.id.recyclerfavorites);
        rv.setLayoutManager(new GridLayoutManager(getContext(),2));
        rv.setAdapter(rvadapter);

        //setting up DB and shared pref storage
        FragmentActivity fa=getActivity();
        assert fa!=null;
        sp=fa.getApplicationContext().getSharedPreferences("favoriteimgs", Context.MODE_PRIVATE);
        RecipeViewModel rvm = new ViewModelProvider.AndroidViewModelFactory(getActivity().getApplication()).create(RecipeViewModel.class);

        //fetch all the favorite recipes from DB
        rvm.getAll().observe(getViewLifecycleOwner(), new Observer<List<RecipeDetails>>() {
            @Override
            public void onChanged(List<RecipeDetails> recipeDetails) {
                List<RecipeSummaryIM> rsim=new ArrayList<>();
                List<Bitmap> bmaps=new ArrayList<>();

                //populate both lists from DB and shared pref
                for(int i=0;i<recipeDetails.size();i++){
                    RecipeDetails rd=recipeDetails.get(i);
                    rsim.add(i, Mapper.mapRecipeDetailsToRecipeSummaryIm(rd));
                    bmaps.add(i, StringToBitMap(sp.getString(rd.getIdMeal(),null)));
                }

                //populate adapter with fresh dataset
                rvadapter.setrecipes(rsim,bmaps);

                //in case nothing really happened
                if(rsim.size()==0){
                    Toast t=Toast.makeText(fa.getApplicationContext(),"You have no favorite recipes yet.",Toast.LENGTH_LONG);
                    t.setGravity(Gravity.BOTTOM,0,0);
                    t.show();
                }
            }
        });
    }


    //utility to get bitmap from base64
    private Bitmap StringToBitMap(String encodedString){
        if(encodedString==null){
            return null;
        }
        try {
            byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(Base64.decode(encodedString, Base64.DEFAULT), 0, encodeByte.length);
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }
}
