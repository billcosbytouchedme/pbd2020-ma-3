package si.uni_lj.fri.pbd.miniapp3.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.adapter.SectionsPagerAdapter;

public class MainActivity extends AppCompatActivity {

    private static final int tabs=2;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set toolbar
        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        configureTabs();
    }

    //sets up the 2 tabs and connects them to fragments with adapter
    private void configureTabs(){
        TabLayout tablayout=findViewById(R.id.tabs);
        ViewPager2 vpager=findViewById(R.id.viewpager2);
        SectionsPagerAdapter secadapter=new SectionsPagerAdapter(this,tabs);
        vpager.setAdapter(secadapter);
        new TabLayoutMediator(tablayout, vpager,
                new TabLayoutMediator.TabConfigurationStrategy() {
                    @Override
                    public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                        switch (position){
                            case 0:
                                tab.setText(R.string.searchtabname);
                                break;
                            case 1:
                                tab.setText(R.string.favoritetabname);
                                break;
                            default:
                                break;
                        }
                    }
                }
        ).attach();
    }
}
